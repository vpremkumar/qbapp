package com.hbo.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.hbo.model.Title;

@Component
public interface TitleRespository extends JpaRepository<Title, Long>{

	public final static String FIND_BY_NAME_QUERY = "SELECT t " + 
            "FROM Title t " +
            "WHERE t.name = :name";

	/**
	 * Find movie title by name
	 * @param name
	 * @return
	 */
	@Query(FIND_BY_NAME_QUERY)
	public Title getTitleByName(@Param("name") String name);
}
