package com.hbo.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.hbo.model.TitleBurst;

@Component
public interface TitleBurstRepository extends JpaRepository<TitleBurst, Integer>{
	public final static String FIND_BY_CHANNEL_MONTH_QUERY = "SELECT t " + 
            "FROM TitleBurst t " +
            "WHERE t.airDate = :airDate "
            + "and t.channel.name = :channel";

	public final static String FIND_BY_MONTH_QUERY = "SELECT t " + 
            "FROM TitleBurst t " +
            "WHERE t.airDate = :airDate ";

	public final static String SUMMARY_QUERY = "select t.qbRating, "+
												"SUM(IF(t.dp = 'Y', 1, 0)), "+
												"SUM(IF(t.dp = 'S', 1, 0)), "+
												"SUM(IF(t.dp = 'N', 1, 0)) "+
												"from TitleBurst t "+
												"group by "+
												"t.qbRating ";

	/**
	 * Find Movie Title by name
	 * @param name
	 * @return
	 */
	@Query(FIND_BY_CHANNEL_MONTH_QUERY)
	public List<TitleBurst> findTitleBurstByChannelAirDate(@Param("channel") String channelName, @Param("airDate") String airDate);

	@Query(FIND_BY_MONTH_QUERY)
	public List<TitleBurst> findTitleBurstByAirDate(@Param("airDate") String monthYear);
	
	}
