package com.hbo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
public class QbAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(QbAppApplication.class, args);
    }
}
