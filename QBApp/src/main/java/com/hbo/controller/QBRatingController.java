package com.hbo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hbo.model.Bucket;
import com.hbo.model.Channel;
import com.hbo.model.TitleBurst;

/**
 *REST service for quality bucket rating app.
 *GET methods are for retrieving the title burst details
 *POST used for updating details
 *DELETE to remove any title burst
 *TODO: Check how to add a new title burst, should there be a UIcomponent associated with this function?
 * @author vandana.premkumar
 *
 */
@RestController
@EnableJpaRepositories
@EnableAutoConfiguration
@ComponentScan
public class QBRatingController {
	
	private final TitleRespository titleRespository;
	
	@Autowired
	public QBRatingController(TitleRespository titleRespository) {
		this.titleRespository = titleRespository;
    }

	/**
	 * Get all the title Bursts with rating, box category, box
	 * @return
	 */
	@RequestMapping(
			method = RequestMethod.GET, 
			value = "/all-titleburst")
	public List<Title> getAllTitleBurstDetails() {
		//TODO : jpa for title burst - lazy load cast details
		//eager load rating, box cat, box and title details since general tab needs to show up on load, 
		//TODO: Recommend business if possible , can we show the general tab on demand because this will improve the performance and we can lazy load the tab details
		return null;
	}
	
	/**
	 * Load the page with title burst details
	 * TODO: What are the default values for channel and month, so that on page load we can load those details and user can then select subsequent page load options 
	 * @param channelName
	 * @param monthYear
	 * @return
	 */
	@RequestMapping(
			method = RequestMethod.GET, 
			value = "/get-title-burst/channel/{channel}/month/{month}")
	public List<TitleBurst> getTitleBurstDetails(@PathVariable String channelName,@PathVariable String monthYear) {
		// TODO call business service which will interact with db and return
		Title title = new Title("Test", "19", "A");
		Channel channel = new Channel(channelName);
		TitleBurst tb = new TitleBurst(new Date(),title,channel);
		List<TitleBurst> tbs = new ArrayList<TitleBurst>();
		tbs.add(tb);
		return tbs;
	}
	
	@RequestMapping(
			method = RequestMethod.GET, 
			value = "/get-titleburst-id/channel/{channel}/month-year/{month}")
	public List<Title> getSummaryTitleBurst(@PathVariable String channel,@PathVariable String month) throws QBException {
		//TODO : Business logic to provide a summary of DP, group by rating, sum of all ratings for a quality burst 
		return null;
	}
	
	/**
	 * TODO: Check with business - If the rating can be updating using UI?
	 * update rating
	 * Can the Box value be updated, if so what are the value constraints?
	 * What is DP ?and B.Cat? can they be modified in UI,if so we will need api's for the same.
	 *  
	 * @param Quality rating that needs to be updated
	 * @return
	 * @throws QBException 
	 */
	@RequestMapping(
			method = RequestMethod.POST, 
			value = "/update-titleburst/title-burst/{titleBurstId}/rating/{rating}")
	public List<Title> updateTitleBurstRating(@PathVariable Long titleBurstId, @PathVariable String rating) throws QBException {
		if(Bucket.valueOf(rating) == null)
			throw new QBException("Invalid Quality Rating");
		//TODO : jpa for title burst - updt method 
		return null;
	}
	
	/**
	 * TODO: Confirm with business - If X means delete the title burst
	 * TODO: Check what needs to be happen if on deletion of all title for a particular timeline
	 *       for a channel, should there be a constraint that at least one title burst be retained?
	 * @param Quality rating that needs to be updated
	 * @return
	 * @throws QBException 
	 */
	@RequestMapping(
			method = RequestMethod.DELETE, 
			value = "/update-titleburst/title-burst/{titleBurstId}")
	public List<Title> deleteTitleBurst(@PathVariable Long titleBurstId) throws QBException {
		//TODO : jpa for title burst - updt method 
		return null;
	}
	
	@RequestMapping(
			method = RequestMethod.GET, 
			value = "/summary-titleburst/title-burst/{titleBurstId}")
	public List<Title> getSummaryTitleBurst(@PathVariable Long titleBurstId) throws QBException {
		//TODO : Business logic to provide a summary of DP, group by rating, sum of all ratings for a quality burst 
		return null;
	}
	
	/**
	 * If we are planning to load the title details on demand, we should have this rest service implemented in order to show the details
	 * @param name
	 * @return
	 */
	@RequestMapping(
			method = RequestMethod.GET, 
			value = "/title/{name}")
	public Title getTitleDetails(@PathVariable String name) {
		PageRequest page1 = new PageRequest(
				  0, 20, Direction.ASC, "name", "releaseDate"
				);
	    return null;
	}
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(QBRatingController.class, args);
	}


}