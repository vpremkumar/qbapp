package com.hbo.controller;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.hbo.model.BaseEntity;


@Entity
@Table(name = "TITLE")
public class Title extends BaseEntity{
	
	public String name;
	public String boxOffice;
	public String burstCategory;
	public Date releaseDate;
	public String mpaa;
	public String licensor;
	public String programClass;

	Title(){
		super();
	}
	public Title(String name, String boxOffice, String burstCategory) {
		super();
		this.name = name;
		this.boxOffice = boxOffice;
		this.burstCategory = burstCategory;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBoxOffice() {
		return boxOffice;
	}

	public void setBoxOffice(String boxOffice) {
		this.boxOffice = boxOffice;
	}

	public String getBurstCategory() {
		return burstCategory;
	}

	public void setBurstCategory(String burstCategory) {
		this.burstCategory = burstCategory;
	}

}
