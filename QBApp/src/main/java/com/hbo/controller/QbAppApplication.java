package com.hbo.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.hbo" })
public class QbAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(QbAppApplication.class, args);
    }
}
