package com.hbo.controller;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface TitleRespository extends JpaRepository<Title, Long>{

	public final static String FIND_BY_NAME_QUERY = "SELECT t " + 
            "FROM Title t " +
            "WHERE t.name = :name";

	/**
	 * Find Movie Title by name
	 * @param name
	 * @return
	 */
	//@Query(FIND_BY_NAME_QUERY)
	//public Title getTitleByName(@Param("name") String name, Pageable page);
}
