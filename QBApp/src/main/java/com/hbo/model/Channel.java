package com.hbo.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Enity class for hbo channel table
 * 
 * @author vandana.premkumar
 *
 */
@Entity
@Table(name = "channel")
public class Channel extends BaseEntity {
	private String name;
	private Set<TitleBurst> titleBursts;
	
	public Channel() {
		super();
	}

	public Channel(String channelName) {
		super();
		this.name = channelName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToMany
	public Set<TitleBurst> getTitleBursts() {
		return titleBursts;
	}

	public void setTitleBursts(Set<TitleBurst> titleBursts) {
		this.titleBursts = titleBursts;
	}	
	
}
