package com.hbo.model;

/**
 * Summary dto for the quality bucket page
 * @author vandana.premkumar
 *
 */
public class TitleBurstSummary {
	String rating;
	int y_count;
	int s_count;
	int n_count;
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public int getY_count() {
		return y_count;
	}
	public void setY_count(int y_count) {
		this.y_count = y_count;
	}
	public int getS_count() {
		return s_count;
	}
	public void setS_count(int s_count) {
		this.s_count = s_count;
	}
	public int getN_count() {
		return n_count;
	}
	public void setN_count(int n_count) {
		this.n_count = n_count;
	}
	
}
