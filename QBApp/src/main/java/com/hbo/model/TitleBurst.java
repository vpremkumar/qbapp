package com.hbo.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity class for title burst.   
 * @author vandana.premkumar
 *
 */
@Entity
@Table(name = "title_burst")
public class TitleBurst extends BaseEntity {

	private String airDate;
	private Title title;
	private Channel channel;
	private String qbRating;
	private String dp;
	private String bcat;
	private String box;
	
	TitleBurst(){
		super();
	}
	
	public TitleBurst(String airDate, Title title, Channel channel) {
		super();
		this.airDate = airDate;
		this.title = title;
		this.channel = channel;
	}

	
	public String getAirDate() {
		return airDate;
	}

	public void setAirDate(String airDate) {
		this.airDate = airDate;
	}

	@ManyToOne
    @JoinColumn(name = "title_id")
	public Title getTitle() {
		return title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}

	@ManyToOne
    @JoinColumn(name = "channel_id")
	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	
	public String getQbRating() {
		return qbRating;
	}

	public void setQbRating(String qbRating) {
		this.qbRating = qbRating;
	}

	public String getDp() {
		return dp;
	}

	public void setDp(String dp) {
		this.dp = dp;
	}

	public String getBcat() {
		return bcat;
	}

	public void setBcat(String bcat) {
		this.bcat = bcat;
	}

	public String getBox() {
		return box;
	}

	public void setBox(String box) {
		this.box = box;
	}

	
	
	
	
	
}
