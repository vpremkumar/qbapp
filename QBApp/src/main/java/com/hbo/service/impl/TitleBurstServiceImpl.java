package com.hbo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hbo.jpa.TitleBurstRepository;
import com.hbo.jpa.TitleRespository;
import com.hbo.model.Title;
import com.hbo.model.TitleBurst;
import com.hbo.model.TitleBurstSummary;
import com.hbo.service.TitleBurstService;

/**
 * Business service implementation for quality bucket application
 * @author vandana.premkumar
 *
 */
@Component
public class TitleBurstServiceImpl implements TitleBurstService {

	private final TitleRespository titleRespository;
	private final TitleBurstRepository titleBirstRespository;
	
	@Autowired
	public TitleBurstServiceImpl(TitleRespository titleRespository, TitleBurstRepository titleBirstRespository){
		this.titleRespository = titleRespository;
		this.titleBirstRespository = titleBirstRespository;
		
	}
	
	/* (non-Javadoc)
	 * @see com.hbo.service.TitleBurstService#getAllTitleBurst()
	 */
	@Override
	public List<TitleBurst> getAllTitleBurst() {
		return titleBirstRespository.findAll();
	}

	/* (non-Javadoc)
	 * @see com.hbo.service.TitleBurstService#findTitleBurstByChannelAirDate(java.lang.String, java.lang.String)
	 */
	@Override
	public List<TitleBurst> findTitleBurstByChannelAirDate(String channelName,String monthYear) {
		return titleBirstRespository.findTitleBurstByChannelAirDate(channelName, monthYear);
	}

	/* (non-Javadoc)
	 * @see com.hbo.service.TitleBurstService#getTitleByName(java.lang.String)
	 */
	@Override
	public Title getTitleByName(String name) {
		return titleRespository.getTitleByName(name);
	}

	/* (non-Javadoc)
	 * @see com.hbo.service.TitleBurstService#findAllTitles()
	 */
	@Override
	public List<Title> findAllTitles() {
		return titleRespository.findAll();
	}

	/* (non-Javadoc)
	 * @see com.hbo.service.TitleBurstService#findTitleBurstByAirDate(java.lang.String)
	 */
	@Override
	public List<TitleBurst> findTitleBurstByAirDate(String monthYear) {
		return titleBirstRespository.findTitleBurstByAirDate(monthYear);
	}

	/* (non-Javadoc)
	 * @see com.hbo.service.TitleBurstService#updateRatingByTitleBurstId(java.lang.Integer, java.lang.String)
	 */
	@Override
	public void updateRatingByTitleBurstId(Integer titleBurstId, String rating) {
		TitleBurst tb = titleBirstRespository.getOne(titleBurstId);
		tb.setQbRating(rating);
		titleBirstRespository.save(tb);
	}

	/* (non-Javadoc)
	 * @see com.hbo.service.TitleBurstService#getRatingSummary()
	 */
	@Override
	public List<TitleBurstSummary> getRatingSummary() {
		//TODO mapping the results and return
		return null;
	}

	/* (non-Javadoc)
	 * @see com.hbo.service.TitleBurstService#getTitleByName(java.lang.Integer)
	 */
	@Override
	public Title getTitleByName(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

}
