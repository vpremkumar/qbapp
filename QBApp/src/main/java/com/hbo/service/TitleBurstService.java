package com.hbo.service;

import java.util.List;

import com.hbo.model.Title;
import com.hbo.model.TitleBurst;
import com.hbo.model.TitleBurstSummary;

/**
 * Blueprint for all the business implementation that is needed
 * @author vandana.premkumar
 *
 */
public interface TitleBurstService {

	/**
	 * Get all the the title burst
	 * @return
	 */
	public List<TitleBurst> getAllTitleBurst();

	/**
	 * find the title bursts by channel and month in which it is aired
	 * @param channelName
	 * @param monthYear
	 * @return
	 */
	public List<TitleBurst> findTitleBurstByChannelAirDate(String channelName,String monthYear);

	/**
	 * Get all title details by name
	 * @param name
	 * @return
	 */
	public Title getTitleByName(String name);
	
	/**
	 * Get all title details by id , this will be faster service because of primary key index 
	 * @param name
	 * @return
	 */
	public Title getTitleByName(Integer id);

	/**
	 * Find all hbo titles
	 * @return
	 */
	public List<Title> findAllTitles();

	/**
	 * Find title burst by month
	 * @param monthYear
	 * @return
	 */
	public List<TitleBurst> findTitleBurstByAirDate(String monthYear);

	/**
	 * Update title burst rating
	 * @param titleBurstId
	 * @param rating
	 */
	public void updateRatingByTitleBurstId(Integer titleBurstId, String rating);

	/**
	 * Get title burst summary by rating and DP
	 * @return
	 */
	public List<TitleBurstSummary> getRatingSummary();

}
