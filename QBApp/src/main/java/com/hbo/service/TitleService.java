package com.hbo.service;

import java.util.List;

import com.hbo.controller.Title;
import com.hbo.model.TitleBurst;

public interface TitleService {

	public List<Title> getAllTitles();
	public Title getTitleByName(String name);
	public List<TitleBurst> getAllTitleBurst();
	public TitleBurst getTitleBurstByName(String name);
}
