package com.hbo.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Base entity that has common attributes for all entities, for now added id
 * TODO: created_on, updated_on needs to be added for tracking purpose
 * @author vandana.premkumar
 *
 */
@MappedSuperclass
public class BaseEntity {
    protected Integer id;


    public void setId(Integer id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

}