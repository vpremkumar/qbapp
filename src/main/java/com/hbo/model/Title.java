package com.hbo.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Title table containing all the hbo titles
 * @author vandana.premkumar
 *
 */
@Entity
@Table(name = "title")
public class Title extends BaseEntity{
	
	public String name;
	public String boxOffice;
	public String burstCategory;
	public Date releaseDate;
	public String mpaa;
	public String licensor;
	public String programClass;
	private Set<TitleBurst> titleBursts;
	
	public Title(){
		super();
	}
	public Title(String name, String boxOffice, String burstCategory) {
		super();
		this.name = name;
		this.boxOffice = boxOffice;
		this.burstCategory = burstCategory;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBoxOffice() {
		return boxOffice;
	}

	public void setBoxOffice(String boxOffice) {
		this.boxOffice = boxOffice;
	}

	public String getBurstCategory() {
		return burstCategory;
	}

	public void setBurstCategory(String burstCategory) {
		this.burstCategory = burstCategory;
	}
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	public String getMpaa() {
		return mpaa;
	}
	public void setMpaa(String mpaa) {
		this.mpaa = mpaa;
	}
	public String getLicensor() {
		return licensor;
	}
	public void setLicensor(String licensor) {
		this.licensor = licensor;
	}
	public String getProgramClass() {
		return programClass;
	}
	public void setProgramClass(String programClass) {
		this.programClass = programClass;
	}
	
	@OneToMany
	public Set<TitleBurst> getTitleBursts() {
		return titleBursts;
	}
	public void setTitleBursts(Set<TitleBurst> titleBursts) {
		this.titleBursts = titleBursts;
	}
	
	

}
