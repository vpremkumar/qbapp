package com.hbo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hbo.model.Rating;
import com.hbo.model.Title;
import com.hbo.model.TitleBurst;
import com.hbo.service.TitleBurstService;

/**
 *REST service for quality bucket rating app.
 *GET methods are for retrieving the title burst details
 *POST used for updating details
 *DELETE to remove any title burst
 *TODO: Check how to add a new title burst, should there be a UIcomponent associated with this function?
 * @author vandana.premkumar
 *
 */
@RestController
@EnableJpaRepositories
@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.hbo" })
public class QBRatingController {
	
	private final TitleBurstService titleBurstService;
	
	@Autowired
	public QBRatingController(TitleBurstService titleBurstService) {
		this.titleBurstService = titleBurstService;
    }
	

	/**
	 * Get all the title Bursts with rating, box category, box
	 * @return
	 */
	@RequestMapping(
			method = RequestMethod.GET, 
			value = "/all-titleburst")
	public List<TitleBurst> getAllTitleBurstDetails() {
		PageRequest page1 = new PageRequest(
				  0, 2, Direction.ASC, "name"
				);
		return titleBurstService.getAllTitleBurst();
	}
	
	/**
	 * Load the page with title burst details
	 * TODO: What are the default values for channel and month, so that on page load we can load those details and user can then select subsequent page load options 
	 * @param channelName
	 * @param monthYear
	 * @return
	 */
	@RequestMapping(
			method = RequestMethod.GET, 
			value = "/get-title-burst/month/{monthYear}/channel/{channelName}")
	public List<TitleBurst> getTitleBurstDetailsByMonthAndChannel(@PathVariable String monthYear,@PathVariable String channelName) {
		// TODO call business service which will interact with db and return
		List<TitleBurst> tbs = titleBurstService.findTitleBurstByChannelAirDate(channelName,monthYear);
		return tbs;
	}
	
	/**
	 * Load the page with title burst details
	 * TODO: What are the default values for channel and month, so that on page load we can load those details and user can then select subsequent page load options 
	 * @param channelName
	 * @param monthYear
	 * @return
	 */
	@RequestMapping(
			method = RequestMethod.GET, 
			value = "/get-title-burst/month/{monthYear}")
	public List<TitleBurst> getTitleBurstDetailsByMonth(@PathVariable String monthYear) {
		// TODO call business service which will interact with db and return
		List<TitleBurst> tbs = titleBurstService.findTitleBurstByAirDate(monthYear);
		return tbs;
	}
	
	/**
	 * TODO: Check with business - If the rating can be updating using UI?
	 * update rating
	 * Can the Box value be updated, if so what are the value constraints?
	 * What is DP ?and B.Cat? can they be modified in UI,if so we will need api's for the same.
	 *  
	 * @param Quality rating that needs to be updated
	 * @return
	 * @throws QBException 
	 */
	@RequestMapping(
			method = RequestMethod.POST, 
			value = "/update-titleburst/title-burst/{titleBurstId}/rating/{rating}")
	public boolean updateTitleBurstRating(@PathVariable Integer titleBurstId, @PathVariable String rating) throws QBException {
		if(Rating.valueOf(rating) == null)
			throw new QBException("Invalid Quality Rating");
		titleBurstService.updateRatingByTitleBurstId(titleBurstId,rating); 
		return true;
	}
	
	/**
	 * TODO: Confirm with business - If X means delete the title burst
	 * TODO: Check what needs to be happen if on deletion of all title for a particular timeline
	 *       for a channel, should there be a constraint that at least one title burst be retained?
	 * @param Bool value stating if delete success
	 * @return
	 * @throws QBException 
	 */
	@RequestMapping(
			method = RequestMethod.DELETE, 
			value = "/delete-titleburst/title-burst/{titleBurstId}")
	public Boolean deleteTitleBurst(@PathVariable Long titleBurstId) throws QBException {
		//TODO : jpa for title burst - updt method 
		return null;
	}
	
	@RequestMapping(
			method = RequestMethod.GET, 
			value = "/summary-titleburst")
	public List<Title> getSummaryTitleBurst() throws QBException {
		titleBurstService.getRatingSummary(); 
		return null;
	}
	
	/**
	 * TODO: If we are planning to load the title details on demand, we should have this rest service implemented in order to show the details
	 * @param name
	 * @return
	 */
	@RequestMapping(
			method = RequestMethod.GET, 
			value = "/title/{name}")
	public Title getTitleDetails(@PathVariable String name) {
		return titleBurstService.getTitleByName(name);
	}
	
	/**
	 * TODO: If we are planning to load the title details on demand, we should have this rest service implemented in order to show the details
	 * @param name
	 * @return
	 */
	@RequestMapping(
			method = RequestMethod.GET, 
			value = "/all-titles")
	public List<Title> findAllTiles() {
		return titleBurstService.findAllTitles();
	}
	
	@RequestMapping("/resource")
	  public Map<String,Object> home() {
	    Map<String,Object> model = new HashMap<String,Object>();
	    model.put("id", UUID.randomUUID().toString());
	    model.put("content", "Hello World");
	    return model;
	  }
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(QBRatingController.class, args);
	}


}