package com.hbo.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hbo.model.Channel;

public interface ChannelRespository  extends JpaRepository<Channel, Long>{

}
